package com.example.multipleclickdemo;

import com.example.multipleclickdemo.Util.MultipleClickListener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button mButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mButton = (Button) findViewById(R.id.btn);
		Util.setMultipleClickListener(mButton, 5, 2000, new MultipleClickListener() {

			@Override
			public void onClick(View v, int clickTimes, long effectTime) {
				Toast.makeText(getApplicationContext(), "你在" + effectTime + "毫秒范围内快速点击了" + clickTimes + "次",
						Toast.LENGTH_LONG).show();
			}

		});
	}
}
